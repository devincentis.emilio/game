/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

let scores,
    roundScore,
    activePlayer,
    diceDom = [...document.querySelectorAll('.dice')],
    gamePlaying,
    sixDice

function init() {
    scores = [0, 0]
    roundScore = 0
    activePlayer = 0
    sixDice = false
    diceDom.map(x => x.style.display = 'none')
    document.getElementById('name-0').textContent = 'Player 1'
    document.getElementById('name-1').textContent = 'Player 2'
    Array.from(document.getElementsByClassName('player-score')).map(x => x.textContent = '0')
    Array.from(document.getElementsByClassName('player-current-score')).map(x => x.textContent = '0')
    Array.from(document.querySelectorAll('.player-0-panel, .player-1-panel')).map(x => x.classList.remove('winner', 'active'))
    document.getElementsByClassName('player-0-panel')[0].classList.add('active')
    gamePlaying = true




}

init()


// roll button  + cambia immagine dado centrale aggiunge il risultato di dice al current 
document.querySelector('.btn-roll').addEventListener('click', function () {
    if (gamePlaying) {
        let dice = Math.floor(Math.random() * 6) + 1
        let dice2 = Math.floor(Math.random() * 6) + 1
        console.log(dice, dice2)
        let diceDom = document.getElementById('diceImg')
        let dice2Dom = document.getElementById('dice2Img')
        diceDom.style.display = 'block'
        diceDom.src = 'dice-' + dice + '.png'
        dice2Dom.style.display = 'block'
        dice2Dom.src = 'dice-' + dice2 + '.png'

        if ((dice === 6 && sixDice) || (dice === 6 && sixDice)) {
            scores[activePlayer] = 0
            document.querySelector('#score-' + activePlayer).textContent = '0'
            nextPlayer()
        }

        if (dice !== 1 && dice2 !== 1) {
            (dice === 6 || dice2 === 6) ? sixDice = true: sixDice = false
            roundScore += dice + dice2
            document.querySelector('#current-' + activePlayer).textContent = roundScore //nel queryselector # si riferisce al id 
        } else {
            nextPlayer()
        }


    }
})

//hold button
document.querySelector('.btn-hold').addEventListener('click', function () {
    if (gamePlaying) {
        scores[activePlayer] += roundScore;
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
        if (scores[activePlayer] >= (document.getElementById('winscore').value > 1 ? document.getElementById('winscore').value : 20)) {
            document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
            diceDom.map(x => x.style.display = 'none')
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            gamePlaying = false;
        } else {

            nextPlayer();
        }
    }
});

//new button
document.querySelector('.btn-new').addEventListener('click', init)

function nextPlayer() {
    //Next player
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    roundScore = 0;
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
    diceDom.map(x => x.style.display = 'none')
    sixDice = 0
}